REST APIs are everywhere, and if you're only using built in functions, they're a pain to consume in LabVIEW.

The MGI RESTful package helps you consume these APIs. The library helps build URLs, generate request bodies, handle return codes, parse headers and convert bodies into LabVIEW data types.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
